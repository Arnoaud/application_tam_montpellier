# Application TAM Montpellier

<strong>Présentation :</strong>

Cette application permet de connaitre à tout moment l'horaire de passage d'un prochain BUS ou TRAMWAY de la ville de MONTPELLIER !
Il vous suffit juste de spécifier la station souhaitée et le numéro de la ligne correspondante.
Il est également possible de ne pas spécifier le numéro de la ligne si l'on ne sait pas !

<strong>Comment utiliser l'image Docker :</strong>

- Télécharger la dernière version de notre image avec la commande : <strong>docker pull dockerdearnaud/application_tam_montpellier:latest</strong>
- Lancer le container avec la commande : <strong>docker run -i -t dockerdearnaud/application_tam_montpellier:latest<strong>
- L'application demande de rentrer deux informations, à vous de taper ce que vous voulez !
- L'application affiche ensuite les horaires si elles sont disponibles en fonction de ce que vous avez indiqué !


# <strong>! ENJOY AND HAVE FUN !<strong>