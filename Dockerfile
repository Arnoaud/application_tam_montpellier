FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD target/appli.jar appli.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/appli.jar"]